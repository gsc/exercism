(ns anagram
  (:use [clojure.string :only [lower-case]])
  )

(defn anagrams-for [word anagrams]
  (let [anagram-for-word? (partial anagram? word)]
    (filter anagram-for-word? anagrams))
)

(defn anagram? [word anagram]
  (let [word (lower-case word)
        anagram (lower-case anagram)]
    (and
     (= (frequencies word) (frequencies anagram))
     (not= word anagram)
     )
    )
)
