(ns robot-name)

(defn new_name []
  (str
   (reduce str (map char (take 2 (repeatedly #(rand-nth (range 65 91))))))
   (reduce str (take 3 (repeatedly #(rand-nth (range 0 9)))))
   )
  )

(defn robot []
  (atom (new_name))
  )

(defn robot-name [machine]
  (deref machine)
  )

(defn reset-name [machine]
  (swap! machine (fn [_] (new_name)))
  )
