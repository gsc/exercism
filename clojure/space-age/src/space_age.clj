(ns space-age)

(def ratios
  {:mercury  0.2408467
  :venus  0.61519726
  :earth 1
  :mars  1.8808158
  :jupiter  11.862615
  :saturn  29.447498
  :uranus  84.016846
  :neptune  164.79132})


(defn planet-years [seconds planet]
  (/ (/ seconds 31557600) (ratios planet))
  )

(defn on-earth [seconds]
  (planet-years seconds :earth)
  )

(defn on-mercury [seconds]
  (planet-years seconds :mercury)
  )

(defn on-venus [seconds]
  (planet-years seconds :venus)
  )

(defn on-mars [seconds]
  (planet-years seconds :mars)
  )

(defn on-jupiter [seconds]
  (planet-years seconds :jupiter)
  )

(defn on-saturn [seconds]
  (planet-years seconds :saturn)
  )

(defn on-uranus [seconds]
  (planet-years seconds :uranus)
  )

(defn on-neptune [seconds]
  (planet-years seconds :neptune)
  )
