(ns binary-search)

(defn middle [array]
  (quot (count array) 2)
  )

(defn search-for [elem array]
  (let [array (vec array)]
    (if (= (nth array (middle array)) elem)
      (middle array)
      (if (<= (count array) 1)
        (throw (Exception. "not found"))
        (if (> (nth array (middle array)) elem)
          (search-for elem (subvec array 0 (middle array)))
          (+ (search-for elem (subvec array (middle array))) (middle array))
          )
        )
      )
    )
  )
