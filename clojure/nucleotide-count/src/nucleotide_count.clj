(ns nucleotide-count)

(def dna-nucleotides #{\A \C \G \T})

(defn count [nucleotide string]
  (do
    (assert (contains? dna-nucleotides nucleotide))
    (clojure.core/count (filter (fn [x] (= nucleotide x)) (seq string)))
    )
  )

(defn nucleotide-counts [strand]
  (into {} (for [x dna-nucleotides] {x (count x strand)}))
  )
