(ns grade-school)

(defn add [db name grade]
  (if (contains? db grade)
    (update db grade (fn [x] (conj x name)))
    (assoc db grade [name])
  )
  )

(defn grade [db grade]
  (db grade [])
  )

(defn- sort-values [db]
  (map vec (map #(sort (second %)) db))
  )

(defn sorted [db]
  (into (sorted-map) (zipmap (keys db) (sort-values db)))
  )


