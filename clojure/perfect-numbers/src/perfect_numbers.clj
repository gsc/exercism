(ns perfect-numbers)

(defn classify [number]
  (if (< number 0)
    (throw (IllegalArgumentException.))
    (let [num_sum (reduce + (factors number))]
      (cond
        (> num_sum number number) :abundant
        (< num_sum number number) :deficient
        (= num_sum number number) :perfect
      )
    )
  )

(defn factors [n]
  (filter #(= (mod n %) 0) (range 1 n))
  )
