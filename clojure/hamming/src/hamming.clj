(ns hamming)

(defn distance [strand1 strand2]
  (if (= (count strand1) (count strand2))
    (count (remove true? (map hamming/nucleotides_equal? strand1 strand2)))
    nil
    )
)

(defn nucleotides_equal? [n1 n2]
  (= n1 n2)
  )
