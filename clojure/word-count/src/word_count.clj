(ns word-count
  (:use [clojure.string :only [replace lower-case split]]))

(defn clean-text [text]
  (replace text #"[^A-Za-z0-9]" " ")
  )

(defn prepro [text]
  (map clean-text (map lower-case (split (clean-text text) #"\s+")))
  )


(defn word-count [phrase]
  (frequencies (prepro phrase))
  )

