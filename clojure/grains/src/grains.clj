(ns grains
  )

(defn square [sq]
  (if (> sq 0)
    (.pow (BigInteger. "2") (- sq 1))
    (throw (Exception. "Square must be positive"))
    )
  )

(defn total []
  (reduce + (map square (range 1 65)))
  )
