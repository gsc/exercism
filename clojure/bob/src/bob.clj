(ns bob
  (:require [clojure.string :as string])
  )

(defn response-for [text]
  (cond
    (string/blank? text) "Fine. Be that way!"
    (and (contains-letters text) (= (string/upper-case text) text)) "Whoa, chill out!"
    (string/ends-with? text "?") "Sure."
    :else "Whatever."
    )
  )

(defn contains-letters [text]
  (if (re-find #"[A-Z]" (string/upper-case text)) true false)
  )

