(ns trinary)

(defn valid-trinary? [number]
  (= (count (filter #(> (Character/digit % 3) -1) number)) (count number))
  )

(defn to-decimal [number]
  (if (or (= (count number) 0)
          (not (valid-trinary? number)))
    0
    (let [digit (Character/digit (first number) 3)
          rest (clojure.string/join (rest number))
          pos (- (count number) 1)]
      (cond
        (> digit 0) (int (+ (* digit (Math/pow 3 pos))  (to-decimal rest)))
        (= digit 0) (to-decimal rest)
        )
      )
    )
  )


