(ns gigasecond)

(import java.util.Calendar)

(defn from [year month day]
  (let [calendar (java.util.GregorianCalendar. year (dec month) day)]
    (do
      (.add calendar Calendar/SECOND (Math/pow 10 9))
      [(.get calendar Calendar/YEAR) (inc (.get calendar Calendar/MONTH)) (.get calendar Calendar/DAY_OF_MONTH)]
    )
  )
)
