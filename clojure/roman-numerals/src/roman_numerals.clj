(ns roman-numerals
  (:require [clojure.string :refer [join]])
  )

(def letters [\M "CM" \D "CD" \C  "XC" \L  "XL" \X "IX" \V "IV" \I])
(def letter-values [1000 900 500 400 100 90 50 40 10 9 5 4 1])

(defn numerals [number]
  (quot_factor number 0)
  )

(defn quot_factor [number num_factor]
  (if (or (> num_factor (count letters)) (= number 0))
    ""
    (let [factor (nth letter-values num_factor)
          letter (nth letters num_factor)
          times (quot number factor)]
      (let [rest (- number (* factor times))
            next_factor (+ num_factor 1)]
        (str (join (repeat times letter)) (quot_factor rest next_factor))
        )
      )
    )
  )
