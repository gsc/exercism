(ns beer-song)

(defn- first-verse [num_bottles]
  (case num_bottles
    1 "1 bottle of beer on the wall, 1 bottle of beer.\n"
    0 "No more bottles of beer on the wall, no more bottles of beer.\n"
    (str num_bottles " bottles of beer on the wall, " num_bottles " bottles of beer.\n")
    )
  )

(defn- second-verse [num_bottles]
  (case num_bottles
    2 "Take one down and pass it around, 1 bottle of beer on the wall.\n"
    1 "Take it down and pass it around, no more bottles of beer on the wall.\n"
    0 "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
    (str "Take one down and pass it around, " (- num_bottles 1) " bottles of beer on the wall.\n")
  )
  )

(defn verse [num_bottles]
  (clojure.string/join "" [(first-verse num_bottles), (second-verse num_bottles)])
  )

(defn sing-verses [start end]
  (clojure.string/join "\n"
   (for [x (reverse (range end (+ start 1)))] (verse x))
   )
  )

(defn sing
  ([start] (sing-verses start  0))
  ([start end] (sing-verses start end))
  )
