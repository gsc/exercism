(ns allergies)

(def allergies-list
  {1 :eggs
   2 :peanuts
   4 :shellfish
   8 :strawberries
   16 :tomatoes
   32 :chocolate
   64 :pollen
   128 :cats}
  )

(defn allergies
  ([number]
   (allergies (mod number 256) 7 []))
  ([number exp acc]
    (if (and (> number 0) (>= exp 0))
      (let [factor (int (Math/pow 2 exp))
            rest (- number factor)]
        (if (>= rest 0)
          (allergies rest (- exp 1) (conj acc factor))
          (allergies number (- exp 1) acc)
          )
        )
      (vec (vals (select-keys allergies-list (sort acc))))
     )
    )
  )

(defn allergic-to? [number allergy]
  (.contains (allergies number) allergy)
  )
