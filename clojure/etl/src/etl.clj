(ns etl
  (:use [clojure.string :only [lower-case]])
  )

(defn transform [words]
  (reduce merge (map (fn[[k v]] (expand v k)) words))
   )

(defn- expand [words score]
  (reduce merge (map #(hash-map % score) (map #(lower-case %) words)))
  )
