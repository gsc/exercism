(ns meetup)

(import java.util.Calendar)

(def teenths (set (range 13 20)))

(defn meetup [month year day position]
  (let [calendar (java.util.GregorianCalendar. year (dec month) 1)]
    (do
      (while (not (and (weekday-match? calendar day) (position-match calendar position)))
        (.add calendar Calendar/DAY_OF_MONTH 1)
        )
      [year month (.get calendar Calendar/DAY_OF_MONTH)]
    )
    )
  )

(defn weekday-match? [calendar day]
  (case day
    :monday (= (.get calendar Calendar/DAY_OF_WEEK) 2)
    :tuesday (= (.get calendar Calendar/DAY_OF_WEEK) 3)
    :wednesday (= (.get calendar Calendar/DAY_OF_WEEK) 4)
    :thursday (= (.get calendar Calendar/DAY_OF_WEEK) 5)
    :friday (= (.get calendar Calendar/DAY_OF_WEEK) 6)
    :saturday (= (.get calendar Calendar/DAY_OF_WEEK) 7)
    :sunday (= (.get calendar Calendar/DAY_OF_WEEK) 1)
    )
  )

(defn position-match [calendar position]
  (let [day (.get calendar Calendar/DAY_OF_MONTH)
        month_days (.getActualMaximum calendar Calendar/DAY_OF_MONTH)]
    (case position
      :first (<= (- day 7) 0)
      :second (and (<= (- day 14) 0) (> day 7))
      :third (and (<= (- day 21) 0) (> day 14))
      :fourth (and (<= (- day 28) 0) (> day 21))
      :last (> (+ day 7) month_days)
      :teenth (contains? teenths day)
      )
    )
  )
