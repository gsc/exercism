(ns rna-transcription
  (:require [clojure.string :as string])
  )

(def rna-nucleotides #{\G \C \T \A})

(defn rna-strand? [strand]
  (every? true? (map (fn [x] (contains? dna-nucleotides x)) (seq strand)))
  )

(defn rna-complement [nucleotide]
  (case nucleotide
    \G \C
    \C \G
    \T \A
    \A \U
    (throw (Exception. "Wrong nucleotide"))
    )
  )

(defn to-rna [strand]
  (do
    (assert (rna-strand? strand))
    (string/join (map rna-complement (seq strand)))
    )
  )


