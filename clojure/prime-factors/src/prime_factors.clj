(ns prime-factors)

(defn of
  ([number]
   (of number 2 []))
  ([number divisor acc]
   (cond
      (< number divisor) acc
      (zero? (rem number divisor)) (recur (/ number divisor) divisor (conj acc divisor))
      :else (recur number (inc divisor) acc)
      )
    )
  )
;; (defn of [num]
;;   (loop [number num
;;          divisor 2
;;          acc []]
;;     (cond
;;       (< number divisor) acc
;;       (zero? (rem number divisor)) (recur (/ number divisor) divisor (conj acc divisor))
;;       :else (recur number (inc divisor) acc)
;;       )
;;     )
;;   )
