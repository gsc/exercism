(ns phone-number)

(defn clean-number [number]
  (clojure.string/replace  number #"[\(\). \-]" "")
  )

(defn number [number]
  (let [number (clean-number number)]
    (if (= (count number) 11)
      (if (= (first number) \1)
        (subs number 1)
        "0000000000"
        )
      (if (= (count number) 10)
        number
        "0000000000"
        )
      )
    )
  )

(defn area-code [num]
  (subs (number num) 0 3)
  )

(defn pretty-print [num]
  (let [num (number num)]
    (str "(" (area-code num) ") " (subs num 3 6) "-" (subs num 6))
    )
  )


