(ns octal)

(defn valid-octal? [number]
  (every? #(not= -1 %)(map #(Character/digit % 8) number))
  )

(defn to-decimal [number]
  (if (valid-octal? number)
    (let [factor (Character/digit (first number) 8)
          rest (rest number)
          exp (- (count number) 1)
          ]
      (if (> exp 0)
        (int (+ (* factor (Math/pow 8 exp)) (to-decimal rest)))
        (int (* factor (Math/pow 8 exp)))
        )
      )
    0
    )
  )
