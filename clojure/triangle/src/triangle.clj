(ns triangle)

(defn inequality? [x y z]
  (and
   (< x (+ y z))
   (< y (+ x z))
   (< z (+ x y))
  )
 )

(defn isosceles? [x y z]
  (or (= x y)
      (= x z)
      (= y z)
  )
  )

(defn equilateral? [x y z]
  (and (= x y) (= y z))
)


(defn type [x y z]
  (if (inequality? x y z)
    (cond
      (equilateral? x y z) :equilateral
      (isosceles? x y z) :isosceles
      :else :scalene
      )
    :illogical
    )
  )

