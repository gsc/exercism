(ns strain)

(defn retain [fun coll]
  (loop [coll coll acc []]
    (let [head (first coll)
          tail (rest coll)]
      (cond
        (= (count coll) 0) acc
        (fun head) (recur tail (concat acc [head]))
        :else (recur tail acc)
        )
      )
    )
  )

(defn discard [fun coll]
    (loop [coll coll acc []]
    (let [head (first coll)
          tail (rest coll)]
      (cond
        (= (count coll) 0) acc
        (fun head) (recur tail acc)
        :else (recur tail (concat acc [head]))
        )
      )
    )
  )
