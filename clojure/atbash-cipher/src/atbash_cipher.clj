(ns atbash-cipher
  (:require [clojure.string :refer [replace join lower-case]])
  )

(def alphabet "abcdefghijklmnopqrstuvwxyz")
(def cipher (zipmap alphabet (join (reverse alphabet))))

(defn encode [text]
  (let [text (clean-text text)]
    (pieces (join (map transpose text)) 5)
    )
  )

(defn- clean-text [text]
  (join (map lower-case (replace text #"[\., ]" "")))
  )

(defn- pieces [text size]
  (let [size (if (> size (count text)) (count text) size)
        piece (subs text 0 size)
        rest (subs text size)]
    (if (> (count rest) 0)
      (join " " [piece (pieces rest size)])
      piece
      )
    )
  )

(defn- is-letter? [character]
  (and (>= (int character) (int \a))  (<= (int character) (int \z)))
  )

(defn- transpose [character]
  (if (is-letter? character)
    (cipher character)
    character
    )
    )

