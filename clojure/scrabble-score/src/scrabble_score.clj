(ns scrabble-score)

(def raw-scores {1 [\A, \E, \I, \O, \U, \L, \N, \R, \S, \T]
             2 [\D, \G]
             3 [\B, \C, \M, \P]
             4 [\F, \H, \V, \W, \Y]
             5 [\K]
             8 [\J, \X]
             10 [\Q, \Z]
             })

(defn expand- [words score]
  (reduce merge (map #(hash-map % score) words))
  )

(defn transform- [words]
  (reduce merge (map (fn[[k v]] (expand v k)) words))
  )

(def score (transform raw-scores))

(defn score-letter [letter]
  (let [letter (first (seq (clojure.string/upper-case letter)))]
    (score letter)
  )
  )

(defn score-word [word]
  (reduce + (map score-letter word))
  )
