(ns leap)

(defn leap-year? [year]
  (and (= 0 (mod year 4))
       (or
        (= 0 (mod year 400))
        (not= 0 (mod year 100))
        )
       )
  )
