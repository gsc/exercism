(ns flatten-array)

(defn flatten [array]
  (remove nil? (filter (complement sequential?) (tree-seq sequential? seq array)))
  )
