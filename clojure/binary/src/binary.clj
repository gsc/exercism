(ns binary)

(defn to-decimal [number]
  (if (= (count number) 0)
    0
    (let [digit (Character/digit (first number) 2)
          rest (rest number)
          pos (- (count number) 1)]
      (case digit
        1 (int (+ (Math/pow 2 pos)  (to-decimal (clojure.string/join rest))))
        0 (to-decimal (clojure.string/join rest))
        )
      )
    )
  )
